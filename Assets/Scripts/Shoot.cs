﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    public Rigidbody _rigidbody;
    public float impulse = 10f;
    public GameObject player;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        _rigidbody.AddForce(player.transform.forward * impulse, ForceMode.Impulse);
	}

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
