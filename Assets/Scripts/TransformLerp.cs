﻿using UnityEngine;
using System.Collections;

public class TransformLerp : MonoBehaviour {
	
	public Transform objetivo;
	
	public bool seguirPosicion;
	public float acercamientoPosicion = 0.5f;
	public bool seguirRotacion;
	public float acercamientoRotacion = 0.5f;
	
	// Update is called once per frame
	void Update ( ) {
	
		if ( seguirPosicion ) {
		
			transform.position = Vector3.Lerp ( 
				transform.position , objetivo.position , acercamientoPosicion );
			
		}
		
		if ( seguirRotacion ) {
			
			transform.rotation = Quaternion.Lerp ( 
				transform.rotation , objetivo.rotation , acercamientoRotacion );
			
		}
		
	}
}
