﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    public int enemyLife;
    public NavMeshAgent agent;

    public Animator anim;

    public int attackDistance;

    public GameObject hitTrigger;
   

    public GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        agent.SetDestination(player.transform.position);
    }

    // Update is called once per frame
    void Update() {


        if ( anim.GetCurrentAnimatorStateInfo ( 0 ).IsName ( "Attack" ) || anim.GetCurrentAnimatorStateInfo(0).IsName("GetHit")) {
            agent.SetDestination(transform.position);
        }
        else
        {
            agent.SetDestination(player.transform.position);
        }

        if ( Vector3.Distance ( transform.position , player.transform.position ) <= attackDistance)
        {
            anim.SetTrigger("Attack");
        }

    }
    private void OnTriggerEnter(Collider other)
    {
  
        if (other.tag == "Shoot") {

            enemyLife -= 1;
            Destroy(other.gameObject);
            anim.SetTrigger("Hit");
            if (enemyLife <= 0) {

                anim.SetTrigger("Death");
                GameController.main.AddScore();
                Destroy(hitTrigger);
                Destroy(agent);
                
                Destroy(this);

            }

        }
    }
}
