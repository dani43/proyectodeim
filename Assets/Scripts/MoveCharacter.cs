﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class MoveCharacter : MonoBehaviour {

    public float speed;
    public float horizontal, vertical;
    public Vector3 move;

    public Animator animator;
    public bool run;
    public bool attack;

    public GameObject shoot;
    public Transform shotSpawn;
    public float fireRate;

    private float nextFire;

    public float hp;
    public float hpMax = 100;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        horizontal = CnInputManager.GetAxis("Horizontal");
        vertical = CnInputManager.GetAxis("Vertical");

        move = Vector3.forward * vertical;
        move += Vector3.right * horizontal;

        transform.Translate(move * speed * Time.deltaTime , Space.World );

        if ( move.magnitude > 0.01f )
        {
            // transform.forward = move;
            transform.forward = Vector3.Slerp(transform.forward, move, 0.25f);
        }

        run = ( horizontal != 0 ) || ( vertical != 0 );
        animator.SetBool("Moving", run);

        attack =(CnInputManager.GetButtonDown("Jump"));
        animator.SetBool("Attack1Trigger", attack);

       

       

    }

    public void Shoot ( )
    {
        Instantiate(shoot, shotSpawn.position, shotSpawn.rotation);
    }

    public void Damage ( float damage ) {
        hp -= damage;
        if ( hp <= 0 )
        {
            animator.SetTrigger("Death");
            Destroy(this);
        }
    }

}
