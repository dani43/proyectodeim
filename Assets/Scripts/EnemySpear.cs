﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpear : MonoBehaviour {

    public GameObject player;
    public int enemyDamage;

    public Animator anim;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");

        anim = GetComponentInParent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            // Obtenemos acceso al script y lo guardamos en una variable
            MoveCharacter scriptMoveCharacter = player.GetComponent<MoveCharacter>();

            if (scriptMoveCharacter)
            {
                scriptMoveCharacter.Damage(enemyDamage);
            }

        }
    }
}
