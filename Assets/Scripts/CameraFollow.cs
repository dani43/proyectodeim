﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject player;
    public GameObject playerPivot;
    public GameObject cameraPivot;
	
	// Update is called once per frame
	void FixedUpdate () {
        // Seguimos al player
        playerPivot.transform.position = player.transform.position + player.transform.forward*2;

        // Seguimiento de la cámara
        transform.position = Vector3.Slerp ( transform.position , cameraPivot.transform.position , 0.1f );

    }
}
