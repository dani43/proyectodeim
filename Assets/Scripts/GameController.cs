﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour {

    public static GameController main;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI lifeText;
    public Image hpImage;
    public int score;
    public GameObject pauseMenu;
    public GameObject deathMenu;

    public MoveCharacter scriptMoveCharacter;

	// Use this for initialization
	void Start () {
        main = this;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape)) {

            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }

         hpImage.fillAmount = scriptMoveCharacter.hp / scriptMoveCharacter.hpMax;
         lifeText.text = "" + scriptMoveCharacter.hp + "/" + scriptMoveCharacter.hpMax;

        if (scriptMoveCharacter.hp <= 0) {

            deathMenu.SetActive(true);
        }

	}

    public void AddScore() {

        score += 1;
        scoreText.text = "" + score;
    }

    public void Resume() {

        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        
    }

    public void GoMenu() {

        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

}
