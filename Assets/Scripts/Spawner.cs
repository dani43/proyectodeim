﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {


    //SpawnerRadial
    /*public GameObject enemy;
    public float radius;
    public float spawnTime;
    public float maxSpawnTime = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        // Contamos hasta que llegue el momento de clonar
        spawnTime += Time.deltaTime;
        if ( spawnTime > maxSpawnTime )
        {
            spawnTime = 0f;

            // Calculamos posición de clonado
            Vector3 position = Random.onUnitSphere;
            position.y = 0f;
            position.Normalize();
            position = position * radius;
            position += transform.position;

            // Clonamos
            Instantiate(enemy, position, Quaternion.identity);
        }



    }*/


    //Spawner nuevo

    public GameObject[] enemys;
    private float spawnTime = 3f;

    private int index;

    //private int spawnLocation;

     void Start()
     {
         Instantiate(enemys[0], new Vector3(-6.5f, 0, 9.3f), Quaternion.identity);

         Invoke("SpawnEnemy", 0);
     }

     void Update()
     {
         if (spawnTime >= 1f)
         {
             spawnTime = spawnTime - 0.02f * Time.deltaTime;
         }

     }

     /*void SpawnEnemy()
     {
         index = Random.Range(0, 3);
         spawnLocation = Random.Range(1, 5);

         if (spawnLocation == 1)
         {
             Instantiate(enemys[index], new Vector3(-20.4f, 0, 51.8f), Quaternion.identity);
         }

         if (spawnLocation == 2)
         {
             Instantiate(enemys[index], new Vector3(-12.8f, 0, 68.9f), Quaternion.identity);
         }

         if (spawnLocation == 3)
         {
             Instantiate(enemys[index], new Vector3(8.15f, 0, 62.82f), Quaternion.identity);
         }

         if (spawnLocation == 4)
         {
             Instantiate(enemys[index], new Vector3(19.5f, 0, 60.5f), Quaternion.identity);
         }

         Invoke("SpawnEnemy", spawnTime);
     }*/

    void SpawnEnemy() {

        index = Random.Range(0, 27);
        switch (index) {

            case 1:
                Instantiate(enemys[0], new Vector3 (-20.4f, 0, 51.8f), Quaternion.identity);
                break;

            case 2:
                Instantiate(enemys[0], new Vector3(-12.8f, 0, 68.9f), Quaternion.identity);
                break;

            case 3:
                Instantiate(enemys[0], new Vector3(8.15f, 0, 62.82f), Quaternion.identity);
                break;

            case 4:
                Instantiate(enemys[0], new Vector3(19.5f, 0, 60.5f), Quaternion.identity);
                break;

            case 5:
                Instantiate(enemys[0], new Vector3(33, 0, 44.7f), Quaternion.identity);
                break;

            case 6:
                Instantiate(enemys[0], new Vector3(50, 0, 29), Quaternion.identity);
                break;

            case 7:
                Instantiate(enemys[0], new Vector3(64.5f, 0, 18), Quaternion.identity);
                break;

            case 8:
                Instantiate(enemys[0], new Vector3(64.5f, 0, 0), Quaternion.identity);
                break;

            case 9:
                Instantiate(enemys[0], new Vector3(61.5f, 0, -21.8f), Quaternion.identity);
                break;

            case 10:
                Instantiate(enemys[0], new Vector3(42, 0, -29f), Quaternion.identity);
                break;

            case 11:
                Instantiate(enemys[0], new Vector3(23, 0, -18.5f), Quaternion.identity);
                break;

            case 12:
                Instantiate(enemys[0], new Vector3(-66, 0, -65), Quaternion.identity);
                break;

            case 13:
                Instantiate(enemys[0], new Vector3(-73, 0, -47.8f), Quaternion.identity);
                break;

            case 14:
                Instantiate(enemys[0], new Vector3(-64, 0, -31), Quaternion.identity);
                break;

            case 15:
                Instantiate(enemys[1], new Vector3(-36.5f, 0, -11), Quaternion.identity);
                break;

            case 16:
                Instantiate(enemys[1], new Vector3(-36.5f, 0, -11), Quaternion.identity);
                break;

            case 17:
                Instantiate(enemys[1], new Vector3(-54, 0, 5.8f), Quaternion.identity);
                break;

            case 18:
                Instantiate(enemys[1], new Vector3(-37, 0, 18.6f), Quaternion.identity);
                break;

            case 19:
                Instantiate(enemys[1], new Vector3(-75.5f, 0, 37.5f), Quaternion.identity);
                break;

            case 20:
                Instantiate(enemys[1], new Vector3(-54, 0, 67.5f), Quaternion.identity);
                break;

            case 21:
                Instantiate(enemys[1], new Vector3(12, 0, -69), Quaternion.identity);
                break;

            case 22:
                Instantiate(enemys[1], new Vector3(20, 0, -36), Quaternion.identity);
                break;

            case 23:
                Instantiate(enemys[2], new Vector3(-15, 0, -88), Quaternion.identity);
                break;

            case 24:
                Instantiate(enemys[2], new Vector3(74.5f, 0, -31.5f), Quaternion.identity);
                break;

            case 25:
                Instantiate(enemys[2], new Vector3(-80, 0, 83), Quaternion.identity);
                break;

            case 26:
                Instantiate(enemys[2], new Vector3(68, 0, 80), Quaternion.identity);
                break;

            case 27:
                Instantiate(enemys[2], new Vector3(81, 0, 32), Quaternion.identity);
                break;

        }

        Invoke("SpawnEnemy", spawnTime);
    }
}
